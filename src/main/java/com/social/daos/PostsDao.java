package com.social.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.social.entities.Posts;

@Repository
public interface PostsDao extends JpaRepository<Posts, Integer> {

}
