package com.social.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.social.entities.Friends;

@Repository
public interface FriendsDao extends JpaRepository<Friends, Integer> {
	@Query(value = "select * from friends where friendid = ? ;", nativeQuery = true)
	Friends findByFriendId(int friendid);
}
