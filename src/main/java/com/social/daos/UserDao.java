package com.social.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.social.entities.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

	@Query(value = "select * from user where userid = ? ;", nativeQuery = true)
	User findByUserId(int userid);
	
	//For Security
	// User findByFullName(String fullname);

}
