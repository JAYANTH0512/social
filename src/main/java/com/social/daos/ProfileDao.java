package com.social.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.social.entities.Profile;

@Repository
public interface ProfileDao extends JpaRepository<Profile, Integer> {
	@Query(value = "select * from profile where profileid = ? ;", nativeQuery = true)
	Profile findByProfileId(int profileid);
}
