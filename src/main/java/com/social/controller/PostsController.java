package com.social.controller;

import static com.social.dto.Response.error;
import static com.social.dto.Response.success;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.social.dto.PostsDto;
import com.social.service.PostsServiceImpl;

@RestController
public class PostsController {

	@Autowired
	private PostsServiceImpl postsservice;

	// Get All Posts
	@GetMapping("/posts/all")
	public ResponseEntity<?> getAllPosts() {
		List<PostsDto> allposts = postsservice.getAllPosts();
		if (allposts != null)
			return success(allposts);
		return error(null);
	}

	// Get Your Post by Post Id
	@GetMapping("/postid/user/{postid}")
	public ResponseEntity<?> getPostByPostId(@PathVariable("postid") int postid) {
		PostsDto post = postsservice.getPostByPostId(postid);
		if (post != null)
			return success(post);
		return error(null);
	}

	// Add Your New Post
	@PostMapping("/post/add")
	public ResponseEntity<?> addNewPost(@RequestBody PostsDto postdto) {
		PostsDto newpost = postsservice.addNewPost(postdto);
		if (newpost != null)
			return success(newpost);
		return error(null);
	}

	// Update Your Post
	@PutMapping("/post/update/{postid}")
	public ResponseEntity<?> updatePost(@RequestBody PostsDto postdto, @PathVariable("postid") int postid) {
		PostsDto post = postsservice.updatePost(postdto, postid);
		if (post != null)
			return success(post);
		return error(null);
	}

	// Delete Existing Post
	@DeleteMapping("/post/delete/{postid}")
	public ResponseEntity<?> deletePost(@PathVariable("postid") int postid) {
		String post = postsservice.deletePost(postid);
		if (post != null)
			return success(post);
		return error(null);
	}
}
