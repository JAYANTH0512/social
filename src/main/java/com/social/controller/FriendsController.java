package com.social.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.social.dto.FriendsDto;
import com.social.service.FriendsServiceImpl;
import static com.social.dto.Response.error;
import static com.social.dto.Response.success;

@RestController
public class FriendsController {

	@Autowired
	private FriendsServiceImpl friendsservice;

	// Get All Friends
	@GetMapping("/friends/all")
	public ResponseEntity<?> getAllFriends() {
		List<FriendsDto> allfriends = friendsservice.getAllFriends();
		if (allfriends != null)
			return success(allfriends);
		return error(null);
	}

	// Get Friend by Friend id
	@GetMapping("/friend/{friendid}")
	public ResponseEntity<?> getFriendById(@PathVariable("friendid") int friendid) {
		FriendsDto friend = friendsservice.getFriendByFriendId(friendid);
		if (friend != null)
			return success(friend);
		return error(null);
	}

	// Add new Friend
	@PostMapping("/friends/add")
	public ResponseEntity<?> addNewFriends(@RequestBody FriendsDto friendsdto) {
		FriendsDto newfriend = friendsservice.addNewFriend(friendsdto);
		if (newfriend != null)
			return success(newfriend);
		return error(null);
	}

	// Update Existing Friend
	@PutMapping("/friends/update/{id}")
	public ResponseEntity<?> updateFriend(@RequestBody FriendsDto friendsdto, @PathVariable("id") int id) {
		FriendsDto existingfriend = friendsservice.updateFriend(friendsdto, id);
		if (existingfriend != null)
			return success(existingfriend);
		return error(null);
	}

	// Delete Existing Friend
	@DeleteMapping("/friends/delete/{id}")
	public ResponseEntity<?> deleteFriend(@PathVariable("id") int id) {
		String friend = friendsservice.deleteFriends(id);
		if (friend != null)
			return success(friend);
		return error(null);
	}
}
