package com.social.controller;

import static com.social.dto.Response.error;
import static com.social.dto.Response.success;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.social.dto.ProfileDto;
import com.social.service.ProfileServiceImpl;

@RestController
public class ProfileController {

	@Autowired
	private ProfileServiceImpl profileservice;

	// Get All Profiles
	@GetMapping("/profiles/all")
	public ResponseEntity<?> getAllProfile() {
		List<ProfileDto> allprofiles = profileservice.getAllProfiles();
		if (allprofiles != null)
			return success(allprofiles);
		return error(null);
	}

	// Get Your Profile by User Id
	@GetMapping("/profile/userid/{userid}")
	public ResponseEntity<?> getProfilebyUserId(@PathVariable("userid") int userid) {
		ProfileDto profile = profileservice.getYourProfilebyUserId(userid);
		if (profile != null)
			return success(profile);
		return error(null);
	}

	// Get Your Profile by Profile Id
	@GetMapping("/profileid/user/{profileid}")
	public ResponseEntity<?> getProfilebyProfileId(@PathVariable("profileid") int profileid) {
		ProfileDto profile = profileservice.getYourProfilebyProfileId(profileid);
		if (profile != null)
			return success(profile);
		return error(null);
	}

	// Add new Profile
	@PostMapping("/profile/add")
	public ResponseEntity<?>  addNewProfile(@RequestBody ProfileDto profiledto) {
		ProfileDto newprofile = profileservice.addYourProfile(profiledto);
		if (newprofile != null)
			return success(newprofile);
		return error(null);
	}

	// Update Existing Profile
	@PutMapping("/profile/update/{userid}")
	public ResponseEntity<?> updateExistingProfile(@RequestBody ProfileDto profiledto, @PathVariable("userid") int userid) {
		ProfileDto existingprofile = profileservice.updateYourProfile(profiledto, userid);
		if (existingprofile != null)
			return success(existingprofile);
		return error(null);
	}

	// Delete Existing Profile
	@DeleteMapping("/profile/delete/{profileid}")
	public ResponseEntity<?> deleteProfile(@PathVariable("profileid") int profileid) {
		String profile = profileservice.deleteProfile(profileid);
		if (profile != null)
			return success(profile);
		return error(null);
	}
}
