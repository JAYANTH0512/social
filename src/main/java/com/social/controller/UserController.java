package com.social.controller;

import static com.social.dto.Response.error;
import static com.social.dto.Response.success;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.social.dto.FriendsDto;
import com.social.dto.PostsDto;
import com.social.dto.UserDto;
import com.social.service.UserServiceImpl;
//import com.social.dto.ProfileDto;
//import com.social.entities.AuthRequest;
//import com.social.util.JwtUtil;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@RestController
public class UserController {
	@Autowired
	private UserServiceImpl userservice;
//	@Autowired
//	private JwtUtil jwtUtil;
//	@Autowired
//	private AuthenticationManager authenticationManager;

	// Get All Users
	@GetMapping("/user/all")
	public ResponseEntity<?> getallUsers() {
		List<UserDto> allusers = userservice.getAllUsers();
		if (allusers != null)
			return success(allusers);
		return error(null);
	}

	// Get User by User id
	@GetMapping("/user/{id}")
	public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
		UserDto user = userservice.getUserByUserId(id);
		if (user != null)
			return success(user);
		return error(null);
	}

	// Get All Your friends by User Id
	@GetMapping("/user/allfriends/{userid}")
	public ResponseEntity<?> getAllFriendsbyUserId(@PathVariable("userid") int userid) {
		List<FriendsDto> allfriends = userservice.getAllFriendsbyUserId(userid);
		if (allfriends != null)
			return success(allfriends);
		return error(null);

	}

	// Get All Your Posts by User Id
	@GetMapping("/user/allposts/{userid}")
	public ResponseEntity<?> getAllPostsByUserId(@PathVariable("userid") int userid) {
		List<PostsDto> allposts = userservice.getAllPostsByUserId(userid);
		if (allposts != null)
			return success(allposts);
		return error(null);
	}

	// Add New User
	@PostMapping("/user/addnewuser")
	public ResponseEntity<?> addNewUser(@RequestBody UserDto user1) {
		UserDto newuser = userservice.addNewUser(user1);
		if (newuser != null)
			return success(newuser);
		return error(null);
	}

	// Update Existing User
	@PutMapping("/user/updateuser/{id}")
	public ResponseEntity<?> updateUserInfo(@RequestBody UserDto userdto, @PathVariable("id") int id) {
		UserDto existinguser = userservice.updateUserInfo(userdto, id);
		if (existinguser != null)
			return success(existinguser);
		return error(null);
	}

	// Delete Existing User
	@DeleteMapping("/user/deleteuser/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") int id) {
		String user = userservice.deleteUser(id);
		if (user != null)
			return success(user);
		return error(null);
	}

//	@PostMapping("/authenticate")
//	public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
//		try {
//			authenticationManager.authenticate(
//					new UsernamePasswordAuthenticationToken(authRequest.getFullName(), authRequest.getPassword()));
//		} catch (Exception ex) {
//			throw new Exception("inavalid username/password");
//		}
//		return jwtUtil.generateToken(authRequest.getFullName());
//	}
}
