package com.social.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "posts")
public class Posts {
	@Id
	@Column
	private int postid;
	@Column
	private String postText;
	@Column
	private String postPhoto;
	@Column
	private String addLocation;
	@Column
	private String activity;
	
	@ManyToOne()
	@JoinColumn(name = "userid")
	@JsonIgnore
	private User user;

	public Posts() {
		super();
	}

	public Posts(int postid, String postText, String postPhoto, String addLocation, String activity, User user) {
		super();
		this.postid = postid;
		this.postText = postText;
		this.postPhoto = postPhoto;
		this.addLocation = addLocation;
		this.activity = activity;
		this.user = user;
	}

	public int getPostid() {
		return postid;
	}

	public void setPostid(int postid) {
		this.postid = postid;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getPostPhoto() {
		return postPhoto;
	}

	public void setPostPhoto(String postPhoto) {
		this.postPhoto = postPhoto;
	}

	public String getAddLocation() {
		return addLocation;
	}

	public void setAddLocation(String addLocation) {
		this.addLocation = addLocation;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Posts [postid=" + postid + ", postText=" + postText + ", postPhoto=" + postPhoto + ", addLocation="
				+ addLocation + ", activity=" + activity + ", user=" + user + "]";
	}
	
}
