package com.social.entities;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "profile")
public class Profile {
//	profileid | schoolName | collegeName | birthDate  | workPlaceName | homeTown | currentlyLivesIn | userid
	@Id
	@Column
	private int profileid;
	@Column
	private String schoolName;
	@Column
	private String collegeName;
	@Column
	private Date birthDate;
	@Column
	private String workPlaceName;
	@Column
	private String homeTown;
	@Column
	private String currentlyLivesIn;
	
	@ManyToOne()
	@JoinColumn(name = "userid")
	@JsonIgnore
	private User user;

	public Profile() {
		super();
	}

	public Profile(int profileid, String schoolName, String collegeName, Date birthDate, String workPlaceName,
			String homeTown, String currentlyLivesIn, User user) {
		super();
		this.profileid = profileid;
		this.schoolName = schoolName;
		this.collegeName = collegeName;
		this.birthDate = birthDate;
		this.workPlaceName = workPlaceName;
		this.homeTown = homeTown;
		this.currentlyLivesIn = currentlyLivesIn;
		this.user = user;
	}

	public int getProfileid() {
		return profileid;
	}

	public void setProfileid(int profileid) {
		this.profileid = profileid;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getWorkPlaceName() {
		return workPlaceName;
	}

	public void setWorkPlaceName(String workPlaceName) {
		this.workPlaceName = workPlaceName;
	}

	public String getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(String homeTown) {
		this.homeTown = homeTown;
	}

	public String getCurrentlyLivesIn() {
		return currentlyLivesIn;
	}

	public void setCurrentlyLivesIn(String currentlyLivesIn) {
		this.currentlyLivesIn = currentlyLivesIn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Profile [profileid=" + profileid + ", schoolName=" + schoolName + ", collegeName=" + collegeName
				+ ", birthDate=" + birthDate + ", workPlaceName=" + workPlaceName + ", homeTown=" + homeTown
				+ ", currentlyLivesIn=" + currentlyLivesIn + ", user=" + user + "]";
	}

}
