package com.social.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "friends")
public class Friends {
	@Id
	@Column
	private int friendid;
	@Column
	private String friendname;
	@Column
	private String friendmobilenum;
	@Column
	private String relationshipstatus;
	
	@ManyToOne()
	@JoinColumn(name = "userid")
	@JsonIgnore
	private User user;

	public Friends() {
		super();
	}

	public Friends(int friendid, String friendname, String friendmobilenum, String relationshipstatus, User user) {
		super();
		this.friendid = friendid;
		this.friendname = friendname;
		this.friendmobilenum = friendmobilenum;
		this.relationshipstatus = relationshipstatus;
		this.user = user;
	}

	public int getFriendid() {
		return friendid;
	}

	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}

	public String getFriendname() {
		return friendname;
	}

	public void setFriendname(String friendname) {
		this.friendname = friendname;
	}

	public String getFriendmobilenum() {
		return friendmobilenum;
	}

	public void setFriendmobilenum(String friendmobilenum) {
		this.friendmobilenum = friendmobilenum;
	}

	public String getRelationshipstatus() {
		return relationshipstatus;
	}

	public void setRelationshipstatus(String relationshipstatus) {
		this.relationshipstatus = relationshipstatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Friends [friendid=" + friendid + ", friendname=" + friendname + ", friendmobilenum=" + friendmobilenum
				+ ", relationshipstatus=" + relationshipstatus + ", user=" + user + "]";
	}

}
