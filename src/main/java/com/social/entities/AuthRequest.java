package com.social.entities;

public class AuthRequest {

    private String fullName;
    private String password;
    
	public AuthRequest() {
		super();
	}
	public AuthRequest(String fullName, String password) {
		super();
		this.fullName = fullName;
		this.password = password;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
    
}