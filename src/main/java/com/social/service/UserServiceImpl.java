package com.social.service;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.social.daos.UserDao;
import com.social.dto.FriendsDto;
import com.social.dto.FriendsDtotoEntity;
import com.social.dto.PostsDto;
import com.social.dto.PostsDtotoEntity;
import com.social.dto.UserDto;
import com.social.dto.UserDtotoEntity;
import com.social.entities.User;
//import java.util.ArrayList;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Service
@Transactional
//implements UserDetailsService
public class UserServiceImpl {

	@Autowired
	private UserDao userdao;

	@Autowired
	private UserDtotoEntity userconverter;

	@Autowired
	private FriendsDtotoEntity friendconverter;

	@Autowired
	private PostsDtotoEntity postsconverter;

//	@Override
//	public UserDetails loadUserByUsername(String fullName) throws UsernameNotFoundException {
//		User user = userdao.findByFullName(fullName);
//		if(user == null)
//		{
//			throw new UsernameNotFoundException("User not found in database");
//		}
//		else	
//		return new org.springframework.security.core.userdetails.User(user.getFullName(), user.getPassword(),
//				new ArrayList<>());
//	}

	// Get All Users Method
	public List<UserDto> getAllUsers() {
		List<User> user = userdao.findAll();
		return user.stream().map(p -> userconverter.toUserDto(p)).collect(Collectors.toList());
	}

	// Get User by User id Method
	public UserDto getUserByUserId(int id) {
		if (userdao.existsById(id)) {
			User user = userdao.findByUserId(id);
			return userconverter.toUserDto(user);
		}
		return null;
	}

	// Get All Your friends by User Id Method
	public List<FriendsDto> getAllFriendsbyUserId(int userid) {
		if (userdao.existsById(userid)) {
			User user = userdao.findById(userid).orElse(null);
			return user.getFriends().stream().map(p -> friendconverter.toFriendsDto(p)).collect(Collectors.toList());
		}
		return null;
	}

	// Get All Your Posts by User Id Method
	public List<PostsDto> getAllPostsByUserId(int userid) {
		if (userdao.existsById(userid)) {
			User user = userdao.findById(userid).orElse(null);
			return user.getPosts().stream().map(p -> postsconverter.toPostsDto(p)).collect(Collectors.toList());
		}
		return null;
	}

	// Add New User Method
	public UserDto addNewUser(UserDto user) {
		if (userdao.existsById(user.getUserid()))
			return null;
		User newuser = userconverter.toUserDtoConverter(user);
		User save = userdao.save(newuser);
		return userconverter.toUserDto(save);
	}

	// Update Existing User Method
	public UserDto updateUserInfo(UserDto userdto, int id) {
		if (userdao.existsById(id)) {
			User updateuser = userdao.findById(id).orElse(null);
			updateuser.setPassword(userdto.getPassword());
			updateuser.setMobileNumber(userdto.getMobileNumber());
			User save = userdao.save(updateuser);
			return userconverter.toUserDto(save);
		}
		return null;
	}

	// Delete Existing User Method
	public String deleteUser(int id) {
		if (userdao.existsById(id)) {
			userdao.deleteById(id);
			return " User sucessfully deleted from Database ";
		}
		return " User failed to Delete from Database ";
	}

}
