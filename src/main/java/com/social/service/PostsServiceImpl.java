package com.social.service;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.social.daos.PostsDao;
import com.social.dto.PostsDto;
import com.social.dto.PostsDtotoEntity;
import com.social.entities.Posts;

@Service
@Transactional
public class PostsServiceImpl {
	@Autowired
	private PostsDao postsdao;
	@Autowired
	private PostsDtotoEntity postsconverter;

	// Get All Posts Method
	public List<PostsDto> getAllPosts() {
		List<Posts> posts = postsdao.findAll();
		return posts.stream().map(p -> postsconverter.toPostsDto(p)).collect(Collectors.toList());
	}

	// Get Your Post by Post Id Method
	public PostsDto getPostByPostId(int postid) {
		Posts post = postsdao.findById(postid).orElse(null);
		return postsconverter.toPostsDto(post);
	}

	// Add Your New Post Method
	public PostsDto addNewPost(PostsDto postdto) {
		if (postsdao.existsById(postdto.getPostid())) {
			return null;
		}
		Posts newpost = postsconverter.toPostsDtoConverter(postdto);
		Posts save = postsdao.save(newpost);
		return postsconverter.toPostsDto(save);
	}

	// Update Your Post Method
	public PostsDto updatePost(PostsDto postdto, int postid) {
		if (postsdao.existsById(postid)) {
			Posts post = postsdao.findById(postid).orElse(null);
			post.setPostText(postdto.getPostText());
			post.setPostPhoto(postdto.getPostPhoto());
			post.setAddLocation(postdto.getAddLocation());
			post.setActivity(postdto.getActivity());
			Posts save = postsdao.save(post);
			return postsconverter.toPostsDto(save);
		}
		return null;
	}

	// Delete Existing Post Method
	public String deletePost(int postid) {
		if (postsdao.existsById(postid)) {
			postsdao.deleteById(postid);
			return " Post sucessfully deleted from Database ";
		}
		return " Post failed to delete from Database ";
	}

}
