package com.social.service;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.social.daos.ProfileDao;
import com.social.dto.ProfileDto;
import com.social.dto.ProfileDtotoEntity;
import com.social.entities.Profile;

@Service
@Transactional
public class ProfileServiceImpl {
	@Autowired
	private ProfileDao profiledao;
	@Autowired
	private ProfileDtotoEntity profileconverter;

	// Get All Profiles Method
	public List<ProfileDto> getAllProfiles() {
		return profiledao.findAll().stream().map(p -> profileconverter.toProfileDto(p)).collect(Collectors.toList());
	}

	// Get Your Profile by User Id Method
	public ProfileDto getYourProfilebyUserId(int userid) {
		if (profiledao.existsById(userid)) {
			Profile user = profiledao.findById(userid).orElse(null);
			return profileconverter.toProfileDto(user);
		}
		return null;
	}

	// Get Your Profile by Profile Id Method
	public ProfileDto getYourProfilebyProfileId(int profileid) {
		if (profiledao.existsById(profileid)) {
			Profile profile = profiledao.findById(profileid).orElse(null);
			return profileconverter.toProfileDto(profile);
		}
		return null;
	}

	// Add new Profile Method
	public ProfileDto addYourProfile(ProfileDto profiledto) {
		if (profiledao.existsById(profiledto.getProfileid())) {
			return null;
		}
		Profile newprofile = profileconverter.toProfileDtoConverter(profiledto);
		Profile save = profiledao.save(newprofile);
		return profileconverter.toProfileDto(save);
	}

	// Update Existing Profile Method
	public ProfileDto updateYourProfile(ProfileDto profiledto, int userid) {
		if (profiledao.existsById(userid)) {
			Profile profile = profiledao.findById(userid).orElse(null);
			profile.setSchoolName(profiledto.getSchoolName());
			profile.setCollegeName(profiledto.getCollegeName());
			profile.setWorkPlaceName(profiledto.getWorkPlaceName());
			profile.setHomeTown(profiledto.getHomeTown());
			profile.setCurrentlyLivesIn(profiledto.getCurrentlyLivesIn());
			Profile save = profiledao.save(profile);
			return profileconverter.toProfileDto(save);
		}
		return null;
	}

	// Delete Existing Profile Method
	public String deleteProfile(int profileid) {
		if (profiledao.existsById(profileid)) {
			profiledao.deleteById(profileid);
			return " Profile sucessfully deleted from Database ";
		}
		return " Profile failed to delete from Database ";
	}
}
