package com.social.service;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.social.daos.FriendsDao;
import com.social.dto.FriendsDto;
import com.social.dto.FriendsDtotoEntity;
import com.social.entities.Friends;

@Service
@Transactional
public class FriendsServiceImpl {

	@Autowired
	private FriendsDao friendsdao;
	@Autowired
	private FriendsDtotoEntity friendsconverter;

	// Get All Friends Method
	public List<FriendsDto> getAllFriends() {
		return friendsdao.findAll().stream().map(p -> friendsconverter.toFriendsDto(p)).collect(Collectors.toList());
	}

	// Get Friend by Friend id Method
	public FriendsDto getFriendByFriendId(int friendid) {
		if (friendsdao.existsById(friendid)) {
			Friends friend = friendsdao.findByFriendId(friendid);
			return friendsconverter.toFriendsDto(friend);
		}
		return null;
	}

	// Add new Friend Method
	public FriendsDto addNewFriend(FriendsDto friendsdto) {
		if (friendsdao.existsById(friendsdto.getFriendid())) {
			return null;
		}
		Friends newfriend = friendsconverter.toFriendsDtoConverter(friendsdto);
		Friends save = friendsdao.save(newfriend);
		return friendsconverter.toFriendsDto(save);
	}

	// Update Existing Friend Method
	public FriendsDto updateFriend(FriendsDto friendsdto, int id) {
		if (friendsdao.existsById(id)) {
			Friends updatefriend = friendsdao.findById(id).orElse(null);
			updatefriend.setRelationshipstatus(friendsdto.getRelationshipstatus());
			updatefriend.setFriendmobilenum(friendsdto.getFriendmobilenum());
			Friends save = friendsdao.save(updatefriend);
			return friendsconverter.toFriendsDto(save);
		}
		return null;
	}

	// Delete Existing Friend Method
	public String deleteFriends(int id) {
		if (friendsdao.existsById(id)) {
			friendsdao.deleteById(id);
			return " Friend sucessfully deleted from Database ";
		}
		return " Friend failed to delete from Database ";
	}

}
