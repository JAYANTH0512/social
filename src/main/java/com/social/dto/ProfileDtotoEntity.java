package com.social.dto;

import org.springframework.stereotype.Component;
import com.social.entities.Profile;
import com.social.entities.User;

@Component
public class ProfileDtotoEntity {
	public ProfileDto toProfileDto(Profile profile) {
		ProfileDto dto = new ProfileDto();
		dto.setProfileid(profile.getProfileid());
		dto.setSchoolName(profile.getSchoolName());
		dto.setCollegeName(profile.getCollegeName());
		dto.setBirthDate(profile.getBirthDate());
		dto.setWorkPlaceName(profile.getWorkPlaceName());
		dto.setHomeTown(profile.getHomeTown());
		dto.setCurrentlyLivesIn(profile.getCurrentlyLivesIn());
		dto.setUserid(profile.getUser().getUserid());
		return dto;
	}

	public Profile toProfileDtoConverter(ProfileDto dto) {
		Profile profile = new Profile();
		profile.setProfileid(dto.getProfileid());
		profile.setSchoolName(dto.getSchoolName());
		profile.setCollegeName(dto.getCollegeName());
		profile.setBirthDate(dto.getBirthDate());
		profile.setWorkPlaceName(dto.getWorkPlaceName());
		profile.setHomeTown(dto.getHomeTown());
		profile.setCurrentlyLivesIn(dto.getCurrentlyLivesIn());
		User user = new User();
		user.setUserid(dto.getUserid());
		profile.setUser(user);
		return profile;
	}
}
