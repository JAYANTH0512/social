package com.social.dto;

import java.sql.Date;

public class ProfileDto {

	private int profileid;
	private String schoolName;
	private String collegeName;
	private Date birthDate;
	private String workPlaceName;
	private String homeTown;
	private String currentlyLivesIn;
	private int userid;

	public ProfileDto() {
		super();
	}

	public ProfileDto(int profileid, String schoolName, String collegeName, Date birthDate, String workPlaceName,
			String homeTown, String currentlyLivesIn, int userid) {
		super();
		this.profileid = profileid;
		this.schoolName = schoolName;
		this.collegeName = collegeName;
		this.birthDate = birthDate;
		this.workPlaceName = workPlaceName;
		this.homeTown = homeTown;
		this.currentlyLivesIn = currentlyLivesIn;
		this.userid = userid;
	}

	public int getProfileid() {
		return profileid;
	}

	public void setProfileid(int profileid) {
		this.profileid = profileid;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getWorkPlaceName() {
		return workPlaceName;
	}

	public void setWorkPlaceName(String workPlaceName) {
		this.workPlaceName = workPlaceName;
	}

	public String getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(String homeTown) {
		this.homeTown = homeTown;
	}

	public String getCurrentlyLivesIn() {
		return currentlyLivesIn;
	}

	public void setCurrentlyLivesIn(String currentlyLivesIn) {
		this.currentlyLivesIn = currentlyLivesIn;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "ProfileDto [profileid=" + profileid + ", schoolName=" + schoolName + ", collegeName=" + collegeName
				+ ", birthDate=" + birthDate + ", workPlaceName=" + workPlaceName + ", homeTown=" + homeTown
				+ ", currentlyLivesIn=" + currentlyLivesIn + ", userid=" + userid + "]";
	}

}
