package com.social.dto;

public class UserDto {
	private int userid;
	private String fullName;
	private String email;
	private String password;
	private String mobileNumber;
	private String relationshipstatus;
	private String role;

	public UserDto() {
		super();
	}

	public UserDto(int userid, String fullName, String email, String password, String mobileNumber,
			String relationshipstatus, String role) {
		super();
		this.userid = userid;
		this.fullName = fullName;
		this.email = email;
		this.password = password;
		this.mobileNumber = mobileNumber;
		this.relationshipstatus = relationshipstatus;
		this.role = role;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRelationshipstatus() {
		return relationshipstatus;
	}

	public void setRelationshipstatus(String relationshipstatus) {
		this.relationshipstatus = relationshipstatus;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserDto [userid=" + userid + ", fullName=" + fullName + ", email=" + email + ", password=" + password
				+ ", mobileNumber=" + mobileNumber + ", relationshipstatus=" + relationshipstatus + ", role=" + role
				+ "]";
	}
}
