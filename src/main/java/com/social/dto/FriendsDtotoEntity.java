package com.social.dto;

import org.springframework.stereotype.Component;
import com.social.entities.Friends;
import com.social.entities.User;

@Component
public class FriendsDtotoEntity {
	public FriendsDto toFriendsDto(Friends friends) {
		FriendsDto dto = new FriendsDto();
		dto.setFriendid(friends.getFriendid());
		dto.setFriendname(friends.getFriendname());
		dto.setFriendmobilenum(friends.getFriendmobilenum());
		dto.setRelationshipstatus(friends.getRelationshipstatus());
		dto.setUserid(friends.getUser().getUserid());
		return dto;
	}

	public Friends toFriendsDtoConverter(FriendsDto dto) {
		Friends friends = new Friends();
		friends.setFriendid(dto.getFriendid());
		friends.setFriendname(dto.getFriendname());
		friends.setFriendmobilenum(dto.getFriendmobilenum());
		friends.setRelationshipstatus(dto.getRelationshipstatus());
		User user = new User();
		user.setUserid(dto.getUserid());
		friends.setUser(user);
		return friends;
	}
}
