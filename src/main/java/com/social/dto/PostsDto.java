package com.social.dto;

public class PostsDto {

	private int postid;
	private String postText;
	private String postPhoto;
	private String addLocation;
	private String activity;
	private int userid;
	
	public PostsDto() {
		super();
	}

	public PostsDto(int postid, String postText, String postPhoto, String addLocation, String activity, int userid) {
		super();
		this.postid = postid;
		this.postText = postText;
		this.postPhoto = postPhoto;
		this.addLocation = addLocation;
		this.activity = activity;
		this.userid = userid;
	}

	public int getPostid() {
		return postid;
	}

	public void setPostid(int postid) {
		this.postid = postid;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getPostPhoto() {
		return postPhoto;
	}

	public void setPostPhoto(String postPhoto) {
		this.postPhoto = postPhoto;
	}

	public String getAddLocation() {
		return addLocation;
	}

	public void setAddLocation(String addLocation) {
		this.addLocation = addLocation;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "PostsDto [postid=" + postid + ", postText=" + postText + ", postPhoto=" + postPhoto + ", addLocation="
				+ addLocation + ", activity=" + activity + ", userid=" + userid + "]";
	}
		
}
