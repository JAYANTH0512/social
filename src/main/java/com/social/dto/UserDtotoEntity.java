package com.social.dto;

import org.springframework.stereotype.Component;
import com.social.entities.User;

@Component
public class UserDtotoEntity {
	public UserDto toUserDto(User user) {
		UserDto dto = new UserDto();
		dto.setUserid(user.getUserid());
		dto.setFullName(user.getFullName());
		dto.setEmail(user.getEmail());
		dto.setPassword(user.getPassword());
		dto.setMobileNumber(user.getMobileNumber());
		dto.setRelationshipstatus(user.getRelationshipstatus());
		dto.setRole(user.getRole());
		return dto;
	}

	public User toUserDtoConverter(UserDto dto) {
		User user = new User();
		user.setUserid(dto.getUserid());
		user.setFullName(dto.getFullName());
		user.setEmail(dto.getEmail());
		user.setPassword(dto.getPassword());
		user.setMobileNumber(dto.getMobileNumber());
		user.setRelationshipstatus(dto.getRelationshipstatus());
		user.setRole(dto.getRole());
		return user;
	}

}
