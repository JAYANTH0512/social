package com.social.dto;

import org.springframework.stereotype.Component;
import com.social.entities.Posts;
import com.social.entities.User;

@Component
public class PostsDtotoEntity {
	public PostsDto toPostsDto(Posts posts) {
		PostsDto dto = new PostsDto();
		dto.setPostid(posts.getPostid());
		dto.setPostText(posts.getPostText());
		dto.setPostPhoto(posts.getPostPhoto());
		dto.setAddLocation(posts.getAddLocation());
		dto.setActivity(posts.getActivity());
		dto.setUserid(posts.getUser().getUserid());
		return dto;
	}

	public Posts toPostsDtoConverter(PostsDto dto) {
		Posts posts = new Posts();
		posts.setPostid(dto.getPostid());
		posts.setPostText(dto.getPostText());
		posts.setPostPhoto(dto.getPostPhoto());
		posts.setAddLocation(dto.getAddLocation());
		posts.setActivity(dto.getActivity());
		User user = new User();
		user.setUserid(dto.getUserid());
		posts.setUser(user);
		return posts;
	}
}