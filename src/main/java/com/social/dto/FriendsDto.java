package com.social.dto;

public class FriendsDto {
	private int friendid;
	private String friendname;
	private String friendmobilenum;
	private String relationshipstatus;
	private int userid;

	public FriendsDto() {
		super();
	}

	public FriendsDto(int friendid, String friendname, String friendmobilenum, String relationshipstatus, int userid) {
		super();
		this.friendid = friendid;
		this.friendname = friendname;
		this.friendmobilenum = friendmobilenum;
		this.relationshipstatus = relationshipstatus;
		this.userid = userid;
	}

	public int getFriendid() {
		return friendid;
	}

	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}

	public String getFriendname() {
		return friendname;
	}

	public void setFriendname(String friendname) {
		this.friendname = friendname;
	}

	public String getFriendmobilenum() {
		return friendmobilenum;
	}

	public void setFriendmobilenum(String friendmobilenum) {
		this.friendmobilenum = friendmobilenum;
	}

	public String getRelationshipstatus() {
		return relationshipstatus;
	}

	public void setRelationshipstatus(String relationshipstatus) {
		this.relationshipstatus = relationshipstatus;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "FriendsDto [friendid=" + friendid + ", friendname=" + friendname + ", friendmobilenum="
				+ friendmobilenum + ", relationshipstatus=" + relationshipstatus + ", userid=" + userid + "]";
	}


}
